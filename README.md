# README #

### About This Repository ###

* An iOS application that mimics a handheld calculator  
* v1.0  
* Swift 2.1.1, Xcode 7.2.1  
* Deployment Target: iOS 9.2  
* Compatible with iPhone, iPad, and iPod touch  
* Supports portrait and landscape orientations  

### How To Set Up ###

* Download the project, open the **Calculator.xcodeproj** file, and run the program.  

### Test Cases ###

* Run the program in the simulator and:  
1. Tap `5`  
2. Tap `=` Display: 5.0  
3. Tap `3`  
4. Tap `+` Display: 8.0  
5. Tap `2`  
6. Tap `/` Display: 4.0  
7. Tap `4`  
8. Tap `*` Display: 16.0  
9. Tap `7`  
10. Tap `-` Display: 9.0  
11. Tap `√` Display: 3.0  
12. Tap `AC` Display: 0.0  
13. Tap `3`  
14. Tap `=`  Display: 3.0  
15. Tap `4`  
16. Tap `+`  Display: 7.0  
17. Tap `6`  
18. Tap `*`  Display: 42.0  
19. Tap `AC`  Display: 0.0  

* **NOTE:** The **=** button is fairly misleading, since it doesn't entirely function as most standard calculator's equal buttons do. This is due to the nature of the stack needing the operands and operations in a certain order. Perhaps a better icon for the equal button would be more intuitive.  

### Implementation Details ###

* The main data structure of the program is a stack that holds operands and operations. The stack is recursively evaluated, getting the correct operands for the correct operations in order to return a result. For example, if 3, 4, +, 6, and * were put on the stack, it would recursively translate to: 6 * (4 + 3).  

### Relevant Source Files ###

#### Model ####
* Functionality.swift  

#### View ####
* Main.storyboard  

#### Controller ####
* ViewController.swift  
