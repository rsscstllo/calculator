//
//  ViewController.swift
//  Calculator
//
//  Created by Ross Castillo on 2/10/16.
//  Copyright © 2016 Ross Castillo. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  
  @IBOutlet weak var displayLabel: UILabel!
  
  var userIsInTheMiddleOfTypingANumber = false
  var functionality = Functionality()
  
  var displayValue: Double {
    get {
      return NSNumberFormatter().numberFromString(displayLabel.text!)!.doubleValue
    } set {
      displayLabel.text = "\(newValue)"
      userIsInTheMiddleOfTypingANumber = false
    }
  }
  
  // MARK: View Life Cycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
  
  // MARK: IBActions
  
  // Linked to all digit buttons
  @IBAction func appendDigit(sender: UIButton) {
    let digit = sender.currentTitle!
    if userIsInTheMiddleOfTypingANumber {
      displayLabel.text = displayLabel.text! + digit
    } else {
      displayLabel.text = digit
      userIsInTheMiddleOfTypingANumber = true
    }
  }
  
  // Linked to all operation buttons
  @IBAction func operate(sender: UIButton) {
    if userIsInTheMiddleOfTypingANumber {
      equals()
    }
    if let operation = sender.currentTitle {
      if let result = functionality.performOperation(operation) {
        displayValue = result
      } else {
        displayValue = 0
      }
    }
  }
  
  // Linked to equals sign button
  @IBAction func equals() {
    userIsInTheMiddleOfTypingANumber = false
    if let result = functionality.pushOperand(displayValue) {
      displayValue = result
    } else {
      displayValue = 0
    }
  }
  
  // Linked to AC button
  @IBAction func clear() {
    userIsInTheMiddleOfTypingANumber = false
    functionality.clear()
    displayValue = 0
  }
  
}
