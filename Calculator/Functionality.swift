//
//  Functionality.swift
//  Calculator
//
//  Created by Ross Castillo on 2/10/16.
//  Copyright © 2016 Ross Castillo. All rights reserved.
//

import Foundation

class Functionality {
  
  // Represent an operand/operation
  private enum Op: CustomStringConvertible {
    case Operand(Double)
    case UnaryOperation(String, Double -> Double)
    case BinaryOperation(String, (Double, Double) -> Double)
    var description: String {
      get {
        switch self {
        case .Operand(let operand): return "\(operand)"
        case .UnaryOperation(let symbol, _): return symbol
        case .BinaryOperation(let symbol, _): return symbol
        }
      }
    }
  }
  
  // Declare and initialize an empty array of type Op
  private var opStack = [Op]()
  
  // Declare and initialize an empty dictionary mapping symbols to their operations
  private var knownOperations = [String: Op]()
  
  // Initialize knownOperations whenever this class is created
  // Enum function parameters with closures that return type Double
  // Can't use built in swift functions for division or minus since order is backwards from pushing to and
  // popping from the stack
  init() {
    knownOperations["*"] = Op.BinaryOperation("*", *)
    knownOperations["/"] = Op.BinaryOperation("/") { $1 / $0 }
    knownOperations["+"] = Op.BinaryOperation("+", +)
    knownOperations["-"] = Op.BinaryOperation("-") { $1 - $0 }
    knownOperations["√"] = Op.UnaryOperation("√", sqrt)
  }
  
  // Recursive function returning a tuple of the result of the evaluation and the rest of the stack that still
  // needs to be evaluated. Parameters passed by value (copied)
  private func evaluate(ops: [Op]) -> (result: Double?, remainingOps:[Op]) {
    // Make sure the stack isn't empty
    if !ops.isEmpty {
      // ops is immutable, so it needs to be copied into a new "var" in order to be mutated
      var remainingOps = ops
      // Now grab the last op off the stack
      let op = remainingOps.removeLast()
      
      switch op {
        // If op is Operand, let that value be assigned to a constant called operand
        // Return the result (the operand) and the remaining ops
      case .Operand(let operand):
        return (operand, remainingOps)
        
      // If op is a unary operation, assign it to a constant called operation
      // Go back into the stack and recurse to get the next op
      // Get the operand from the tuple using "if let" to change it from optional to double
      // Return the operation (with its operand) and the remaining ops (after recursing)
      case .UnaryOperation(_, let operation):
        let operandEvaluation = evaluate(remainingOps)  // Recursive step for operand
        if let operand = operandEvaluation.result {
          return (operation(operand), operandEvaluation.remainingOps)
        }
      
      // Similar to above, but have to recurse twice because there are two operands
      case .BinaryOperation(_, let operation):
        let op1Evaluation = evaluate(remainingOps)  // Recursive step for operand1
        if let operand1 = op1Evaluation.result {
          let op2Evaluation = evaluate(op1Evaluation.remainingOps)  // Recursive step for operand2
          if let operand2 = op2Evaluation.result {
            return (operation(operand1, operand2), op2Evaluation.remainingOps)
          }
        }
      }
    }
    // If evaluation fails, return nil
    return (nil, ops)
  }
  
  // Let the client properly evaluate the stack of operations and operands
  // Optional return type for use-case of evaluating operations without operand(s) returning nil
  func evaluate() -> Double? {
    let (result, remainder) = evaluate(opStack)
    print("\(opStack) = \(result) with \(remainder) left over")
    return result
  }
  
  // Push an operand on to the stack by creating an enum value (Op) from the given parameter
  func pushOperand(operand: Double) -> Double? {
    opStack.append(Op.Operand(operand))
    return evaluate()
  }
  
  // Perform an operation specified by its symbol
  func performOperation(symbol: String) -> Double? {
    // Push the operation to the stack if it's in the dictionary
    if let operation = knownOperations[symbol] {
      opStack.append(operation)
    }
    return evaluate()
  }
  
  // Clear info
  func clear() {
    opStack.removeAll()
  }
  
}
